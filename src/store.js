import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export const store = new Vuex.Store({

    state: {
        roladeskAPIadd: {link: 'http://server.roladesk.teamorange.tech/api/'},
        appTitle: ' | Roladesk',
        successMessage: 'Successful!',
        settings: { admin: '1' }
    },
    getters: {
        roladeskAPIget(state) {
            return state.roladeskAPIadd.link;
        },
        appTitle(state){
            return state.appTitle;
        },
        settingsRoleId(state){
            return state.settings.admin;
        },                       
    }
})