import Vue from 'vue';
import VueRouter from 'vue-router';
import Meta from 'vue-meta';

import App from '../App.vue';
import Panel from '../components/panel/Panel.vue';
import DashboardFilter from '../components/panel/dashboard/DashboardFilter.vue';
import Users from '../components/panel/user/Users.vue';
import FilterActivityLog from '../components/panel/log/FilterActivityLog.vue';
import Permissions from '../components/panel/roles-permissions/Permissions.vue';
import Roles from '../components/panel/roles-permissions/Roles.vue';
import General from '../components/panel/settings/General.vue';
import Registrations from '../components/panel/settings/Auth-Registration.vue';
import Notifications from '../components/panel/settings/Notifications.vue';
import Profile from '../components/panel/profile/Profile.vue';
import ActiveSessions from '../components/panel/profile/ActiveSessions.vue';
import Auth from '../components/auth/Auth.vue';
import Login from '../components/auth/Login.vue';
import Initialize from '../components/auth/Initialize.vue';
import Register from '../components/auth/Register.vue';
import Confirmation from '../components/auth/Confirmation.vue';

Vue.use(VueRouter);
Vue.use(Meta);

export default new VueRouter({
    mode: 'history',
    routes: [    
    { path: '/panel', redirect: { name: 'dashboard' } , name: Panel, component: Panel,
          beforeEnter: (to, from, next) => {
            if (localStorage.getItem("token")) {
                  next();
                } else {
                  next({ path: '/login' });
                }
          },
       children: [
                {
                  path: 'dashboard',
                  name: 'dashboard',
                  component: DashboardFilter
                },
                {
                  path: 'user',
                  name: 'users',
                  component: Users
                },
                {
                  path: 'activity',
                  name: 'adminactivitylog',
                  component: FilterActivityLog
                },
                {
                  path: 'roles',
                  name: 'roles',
                  component: Roles
                },
                {
                  path: 'permissions',
                  name: 'permissions',
                  component: Permissions
                },
                {
                  path: 'general',
                  name: 'general',
                  component: General
                },
                {
                  path: 'registration',
                  name: 'registrations',
                  component: Registrations
                },
                {
                  path: 'notifications',
                  name: 'notifications',
                  component: Notifications
                },
                {
                  path: 'profile',
                  name: 'profile',
                  component: Profile
                },
                {
                  path: 'profile/sessions',
                  name: 'activeSessions',
                  component: ActiveSessions
                },
                {
                  path: 'profile/activity',
                  name: 'useractivitylog',
                  component: FilterActivityLog
                }
              ]
      },
      {path: '/', redirect: { name: 'login' } , name: 'Auth', component: Auth,
       children: [       
        {
          path: 'login',
          name: 'login',
          component: Login
        },
        {
          path: 'initialize',
          name: 'initialize',
          component: Initialize,
          beforeEnter: (to, from, next) => {
            if (localStorage.getItem("token")) {
                  next();
                } else {
                  next({ name: 'login' });
                }
          } 
        },
        {
          path: 'register',
          name: 'register',
          component: Register,         
        },
        {
          path: 'confirmation',
          name: 'confirmation',
          component: Confirmation,
          beforeEnter: (to, from, next) => {
            if (localStorage.getItem("token")) {
                  next();
                } else {
                  next({ name: 'login' });
                }
          } 
        }
      ]}
    ]
  });