import Vue from 'vue';
import App from './App.vue';
import router from './router/routes.js';
import axios from 'axios';
import { store } from './store';
import toastr from "toastr";
import Gravatar from 'vue-gravatar';
Vue.component('v-gravatar', Gravatar);

require('bootstrap/dist/css/bootstrap.min.css');
import jQuery from 'jquery';
global.jQuery = jQuery;
require('popper.js/dist/umd/popper');
require('bootstrap/dist/js/bootstrap');
require('toastr/build/toastr.min.js');
require('toastr/build/toastr.min.css');

// Enable global popover
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});
$('.popover-dismiss').popover({
  trigger: 'focus'
})

// Enable global tooltip
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});

// toastr.js Otions
toastr.options = {
    "closeButton": true,
    "positionClass": "toast-bottom-right"
}

Vue.component('vs-crumbs',
    {
	template:
		'<ul class="vs-crumbs" v-if="crumbs.length" style="list-style:none; margin:0; padding:0; display:inline-block; ">' +
			'<li v-for="(crumb, i) in crumbs" style="display:inline-block">' +
				'<router-link v-if="i < crumbs.length-1" :to="crumb.path" style="text-decoration:none;">{{ crumb.name }}</router-link>' +
				'<span class=last v-else style="cursor:default">{{ crumb.name }}</span>' +
            '</li>' + 
        '</ul>',
	props: { root: String },
	computed:
	{
		crumbs: function()
		{
			var path = '', title = (this.root || 'Home');
			var cs = [ { name: title, path: '/panel'} ]; if(!this.$route) return [];
			var r = (this.$route.path                        ).split('/');
			var m = (this.$route.matched[0].meta.crumbs || '').split('/');
			for(var i = 1; i < r.length; i++)
			{
				var name = (m[i] || r[i]); if(r[i] == '') continue;

				title += ' : '+ name;
				path  += '/'  + name;
		
				cs.push({ name: name, path: path });
			}
			window.document.title = title;
			return cs;
		}
    }
});


const app = new Vue({
    el: '#app',
    store: store,
    router,
    render: h => h(App)
});